var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var rentSchema = new Schema({
    client: { type: String },
    car: { type: String },
    price: { type: Number },
    days: { type: String },
});

module.exports = mongoose.model("rents", rentSchema);