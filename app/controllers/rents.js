var mongoose = require("mongoose"),
  Model = require("../models/rent");

exports.getAll = async (req, res, next) => {
  var res = await Model.find({})
  return res;
};

exports.find = async (req, res, next) => {

};

exports.delete = async (req, res, next) => {
  var incDel = {
    _id: req.params.id,
  };
  Model.deleteOne(incDel, function (err) {
    if (err) return handleError(err);
  });
};

exports.add = async (req, res, next) => {
  var el = {
    car: req.body.car,
    client: req.body.client,
    days: req.body.days,
    price: 100,
  };
  var newForm = new Model(el);
  newForm.save((err, res) => {
    if (err) console.log(err);
    console.log("Alquiler guardado en la DB");
    return res;
  });
};