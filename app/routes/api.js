var path = require("path"),
  express = require("express"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));

/* CARS API */

//Obtener datos de car

router.route('/cars/new').get(async (req, res, next) => {
  var carList = await carsCtrl.getAll();
  console.log(carList);
  res.send(carList);
});

//Obtener datos de el modelo proporcionado 

router.route('/cars/new/:brand').get(async (req, res, next) => {
  var carList = await carsCtrl.find(req.params.brand);
  console.log(carList);
  res.send(carList);
});

//Guardar datos de car 
router.post('/cars/save/:id/:brand/:model/:price', async (req, res) => {
  carsCtrl.add(req, res);
  console.log("Coche añadido");
  res.redirect("/api/rent/list");
});

//Eliminar datos de car
router.route('/cars/delete/:id').get(async (req, res, next) => {
  carsCtrl.delete(req, res, next);
  console.log("Coche eliminado");
  res.redirect("/api/rent/list");
});

/* CLIENTS API */

//Obtener datos de clientes

router.route('/client/new').get(async (req, res, next) => {
  var clientsList = await clientsCtrl.getAll();
  console.log(clientsList);
  res.send(clientsList);
});

//Guardar datos de cliente 
router.post('/client/save/:id/:name/:surname', async (req, res) => {
  clientsCtrl.add(req, res);
  console.log("Cliente añadido");
  res.send("/api/rent/list");
});

//Eliminar datos de cliente
router.route('/client/delete/:id').get(async (req, res, next) => {
  clientsCtrl.delete(req, res, next);
  console.log("Cliente eliminado");
  res.redirect("/api/rent/list");
});

/* RENTS API */

// Cargar el formulario
router.route('/rent/new').get(async (req, res, next) => {
  var result = await rentsCtrl.getAll();
  var carList = await carsCtrl.getAll();
  var clientsList = await clientsCtrl.getAll();
  console.log(result);
  res.render("form", { result: result, cars: carList, clients: clientsList });
});

//Guardar datos del formulario
router.post('/rent/save', async (req, res) => {
  rentsCtrl.add(req, res);
  res.redirect("/api/rent/list");
});

// Cargar página de la tabla 
router.route('/rent/list').get(async (req, res, next) => {
  var result = await rentsCtrl.getAll();
  console.log(result);
  res.render("list", { rents: result });
});

// Eliminar una rent
router.route('/rent/delete/:id').get(async (req, res, next) => {
  rentsCtrl.delete(req, res, next);
  res.redirect("/api/rent/list");
});

module.exports = router;
